package gitlabTests;


import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Keys;


import static com.codeborne.selenide.Selenide.$x;

@Tag("UI")
public class UiTests {

    @BeforeAll
    public static void setUp(){
        Configuration.headless = true;
    }

    @BeforeEach
    public void openPage(){
        Selenide.open("https://simplecalc.ru/");
    }

    private void assertAnswer(String answer){

        Assertions.assertEquals("4", answer);
    }

    @Test
    public void calcPlusTest() {
        $x("//div[@id=\"button_opr2-4122\"]").click();
        $x("//div[@id=\"button_oprpl-412pl\"]").click();
        $x("//*[@id=\"button_opr2-4122\"]").click();
        $x("//div[@id=\"button_oprrv-412rv\"]").click();
        assertAnswer($x("//div[@id=\"result\"]").getText());
    }

    @Test
    public void calcPlusTest2() {
        $x("//div[@id=\"button_opr1-4121\"]").click();
        $x("//div[@id=\"button_oprpl-412pl\"]").click();
        $x("//*[@id=\"button_opr3-4123\"]").click();
        $x("//div[@id=\"button_oprrv-412rv\"]").click();
        assertAnswer($x("//div[@id=\"result\"]").getText());
    }


    @Test
    public void calcMinusTest() {
        $x("//div[@id=\"button_opr6-4126\"]").click();
        $x("//div[@id=\"button_oprmi-412mi\"]").click();
        $x("//*[@id=\"button_opr2-4122\"]").click();
        $x("//div[@id=\"button_oprrv-412rv\"]").click();
        assertAnswer($x("//div[@id=\"result\"]").getText());
    }

    @Test
    public void calcMultipyTest() {
        $x("//div[@id=\"button_opr2-4122\"]").click();
        $x("//div[@id=\"button_oprum-412um\"]").click();
        $x("//*[@id=\"button_opr2-4122\"]").click();
        $x("//div[@id=\"button_oprrv-412rv\"]").click();
        assertAnswer($x("//div[@id=\"result\"]").getText());
    }

    @Test
    public void calcDevideTest() {
        $x("//div[@id=\"button_opr8-4128\"]").click();
        $x("//div[@id=\"button_oprde-412de\"]").click();
        $x("//*[@id=\"button_opr2-4122\"]").click();
        $x("//div[@id=\"button_oprrv-412rv\"]").click();
        assertAnswer($x("//div[@id=\"result\"]").getText());
    }
}
